/*const mysql = require('mysql');

const connection = mysql.createConnection({
	host     : 'localhost',
	user     : 'root',
	password : '',
	database : 'noderegister'
});*/

const Pool = require('pg').Pool
const connection = new Pool({
	user: 'postgres',
    host: 'localhost',
    database: 'warehouse_db',
    password: 'root',
    port: 5432,
})
module.exports = { connection }