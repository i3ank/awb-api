const express = require('express');
const router = express.Router();
const { validation, schema } = require('../validator/users');
const multer = require('multer');
const upload = multer({ dest: 'uploads/'});
const fs = require('fs');
const md5 = require('md5');
const Pool = require('pg').Pool
const {  connection } = require('../config/db');

router.route('/')
    .get((req, res, next) => {
        req.renderPage = "pages/403"
        next()
  }); 

  router.route('/create')
  .post(type,(req, res) => { 
      const Joi = require('joi');

      const data = req.body;

      const schema = Joi.object().keys({
          username: Joi.string().min(3).max(30).required(),
          firstname: Joi.string().required(),
          lastname: Joi.string().required(),
          address1: Joi.string().required(),
          telephone:Joi.string().min(6).max(10).required(),
          city: Joi.string().required(),
          state: Joi.string().required(),
          country: Joi.string().required(),
          email: Joi.string().email().required(),
          postcode:Joi.string().min(1).max(5).required(),

      });
      let password_hash = md5(req.body.password);
      Joi.validate(data, schema, (err, value) => {
          if (err) {
              return res.json({ message: "เกิดข้อผิดพลาด" });
          } else {
                      connection.query('SELECT username FROM distributor_login WHERE telephone = $1 or email = $2', [req.body.telephone, req.body.email], function (err, result) {
                          if (err) {
                              throw err
                            }
                            if (result.rows) { 
                              if(result.rows.length > 0) {
          
                                  return res.status(202).json({ message: "เบอร์โทรศัพท์ หรือ อีเมล์ นี้ถูกใช้แล้ว" });
          
                              } else {
                                if (req.file) {                                
                                    var tmp_path = req.file.path;
                                    var target_path = 'uploads/' + req.file.originalname;
                                    var src = fs.createReadStream(tmp_path);
                                    var dest = fs.createWriteStream(target_path);
                                    src.pipe(dest);
                                    src.on('end', function() { 
                                        
                                        connection.query('INSERT INTO distributor_file (username, file) VALUES ($1, $2)', [req.body.email ,target_path], (error, results) => {
                                            if (error) {
                                              throw error
                                            }
                                        });
                
                                    });
                                    src.on('error', function(err) { 
                                        return res.status(202).json({ message: "เกิดข้อผิดพลาด" });
                                    });
                                }
                                                  
                                    connection.query('INSERT INTO distributor_address (username, address1, city, state, country, postcode, date_added) VALUES ($1, $2, $3, $4, $5, $6, $7)', [req.body.email, req.body.address1, req.body.city, req.body.state, req.body.country, req.body.postcode, new Date()], (address, results) => {
                                      if (address) { throw address }
                                        connection.query('INSERT INTO distributor_code (username, date_added) VALUES ($1, $2)', [req.body.email, new Date()], (code, results) => {
                                            if (code) { throw code }
                                            connection.query('INSERT INTO distributor_email (username, email, date_added) VALUES ($1, $2, $3)', [req.body.email, req.body.email, new Date()], (email, results) => {
                                                if (email) { throw email }
                                                connection.query('INSERT INTO distributor_level (username, level) VALUES ($1, $2)', [req.body.email, req.body.distributor], (level, results) => {
                                                    if (level) { throw level }
                                                    connection.query('INSERT INTO distributor_login (username, password, firstname, lastname, status, date_added) VALUES ($1, $2, $3, $4, $5, $6)', [req.body.email, password_hash, req.body.firstname, req.body.lastname, 0, new Date()], (login, results) => {
                                                        if (login) { throw login }
                                                        connection.query('INSERT INTO distributor_parent (username, username_root, date_added) VALUES ($1, $2, $3)', [req.body.email, req.body.parent_code, new Date()], (parent, results) => {
                                                            if (parent) { throw parent }
                                                            connection.query('INSERT INTO distributor_telephone (username, telephone, date_added) VALUES ($1, $2, $3)', [req.body.email, req.body.telephone, new Date()], (telephone, results) => {
                                                                if (telephone) { throw telephone }
                                                                return res.status(200).json({ message: "success" });
                                                            });
                                                        });
                                                    });
                                                });
                                            });
                                        });
                                    });
                            }
                              
                          } else {
          
                            return res.status(400).json({ message: "เกิดข้อผิดพลาดในระบบ" });
          
                              //return res.render(req.renderPage);
          
                          }
                        });
          }
      });
  })

module.exports = router