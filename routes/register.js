const express = require('express');
const router = express.Router();
const { validation, schema } = require('../validator/users');
const multer = require('multer');
const upload = multer({ dest: 'uploads/'});
const fs = require('fs');
const md5 = require('md5');
const Pool = require('pg').Pool
const {  connection } = require('../config/db');

var type = upload.single('image');
router.route('/')
    .get((req, res) => {
    res.json({ message: "Welcome to bezkoder application." });
  }); 
router.route('/create')
    .post((req, res) => { 


        const Joi = require('joi');

        const data = req.body;

        const schema = Joi.object().keys({
            username: Joi.string().min(3).max(30).required(),
            firstname: Joi.string().min(6).max(30).required(),
            lastname: Joi.string().min(3).max(30).required(),
            address1: Joi.string().email().required(),
            address2: Joi.string().email().required(),
            telephone:Joi.string().min(6).max(10).required(),
            city: Joi.string().email().required(),
            state: Joi.string().email().required(),
            country: Joi.string().email().required(),
            email:Joi.string().allow('', null),
            postcode:Joi.string().min(1).max(5).required(),

        });

        Joi.validate(data, schema, (err, value) => {
            if (err) {
                res.status(204).json('เกิดข้อผิดพลาด');
            } else {
                        connection.query('SELECT username FROM customers WHERE username = $1', [req.body.username], function (err, result) {
                            if (err) {
                                throw err
                              }
                              if (result.rows) { 
                                if(result.rows.length > 0) {
            
                                    res.status(204).json('มี Username นี้ในระบบแล้ว');
            
                                } else {
                                    
                                    let password_hash = md5(req.body.password);

                                    connection.query('INSERT INTO cumtomers (firstname, lastname, username, telephone, email, address1, address2, city, state, country, postcode) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11)', [req.body.firstname, req.body.lastname, req.body.username, req.body.telephone, req.body.email, req.body.address1, req.body.address2, req.body.city, req.body.state, req.body.country, req.body.postcode, ], (error, results) => {
                                        if (error) {
                                          throw error
                                        }
                                        res.status(200).json({results});
                                    });
                                }
                                
                            } else {
            
                                res.status(204).json('เกิดข้อผิดพลาด');
            
                                //return res.render(req.renderPage);
            
                            }
                          });
            }
        });
    })


module.exports = router